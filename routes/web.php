<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/app')->group(function(){
	Route::get('/', function(){
		return view('app');
	})->name('app');

	Route::get('/user', function(){
		return view('user');
	})->name('app.user');

	Route::get('/profile', function(){
		return view('profile');
	})->name('app.profile');


});
/*---------------------------------------*/

Route::post('/requeisicoes', function(Request $request) {
	return 'Hello POST';
    //
});

Route::get('produtos', function(){
	return view('produtos.index');
})->name('produtos');
Route::get('departamentos', function(){
	return view('departamentos.index');
})->name('departamentos');
Route::get('nome', 'MeuCotrolador@getNome');
Route::get('idade', 'MeuCotrolador@getIdade');
Route::get('multiplicar/{n1}/{n2}', 'MeuCotrolador@multiplicar');


Route::resource('clientes', 'ClienteControlador');

Route::get('opcoes/{opcao?}', function($opcao=null){
	return view('outros.opcoes', compact(['opcao']));
})->name('opcoes');

Route::get('bootstrap', function() {
    return view('outros.exemplo');
});