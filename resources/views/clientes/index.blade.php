@extends('layouts.principal')

@section('titulo', 'Cliente')

@section('conteudo')

<h3>{{$titulo}}</h3>
<a href="{{ route('clientes.create') }}">Novo Cliente</a>

	@if(count($clientes)>0)
		
		<ul>
			@foreach ($clientes as $c)
				{{-- expr --}}
				<li>
					{{ $c['id'] }}:{{ $c['nome'] }} ->
					<a href="{{ route('clientes.show', $c['id']) }}">Info</a>
					<a href="{{ route('clientes.edit', $c['id']) }}">Edit</a>
					<form action="{{ route('clientes.destroy', $c['id']) }}" method="POST">
						@csrf
						@method('DELETE')
						<input type="submit" name="Apagar" value="Apagar">
					</form>
				</li>
				{{-- {{var_dump($loop)}} --}}
			@endforeach
		</ul>
	@else
		<h4>não existe usuários cadastrados</h4>
	@endif

@endsection