@extends('layouts.principal')

@section('titulo', 'opcoes')

@section('conteudo')

	<div class="options">
		<ul>
			<li><a class="warning" href="{{ route('opcoes',1) }}">warning</a></li>
			<li><a class="info"    href="{{ route('opcoes',2) }}">info</a></li>
			<li><a class="success" href="{{ route('opcoes',3) }}">success</a></li>
			<li><a class="error"   href="{{ route('opcoes',4) }}">error</a></li>
		</ul>
</div>

@if (isset($opcao))

	@switch($opcao)
	    @case(1)	        
			@alerta( ['titulo'=>'erro fatal', 'tipo'=>'warning'])
				<p><strong>Warning</strong></p>
				<p>ocorreu um erro inesperado</p>
			@endalerta
	        @break
		@case(2)	        
			@alerta( ['titulo'=>'erro fatal', 'tipo'=>'info'])
				<p><strong>Info</strong></p>
				<p>ocorreu um erro inesperado</p>
			@endalerta
	        @break
		@case(3)	        
			@alerta( ['titulo'=>'erro fatal', 'tipo'=>'success'])
				<p><strong>Success</strong></p>
				<p>ocorreu um erro inesperado</p>
			@endalerta
	        @break
		@case(4)	        
			@alerta( ['titulo'=>'erro fatal', 'tipo'=>'error'])
				<p><strong>Error</strong></p>
				<p>ocorreu um erro inesperado</p>
			@endalerta
	        @break
		
	    {{-- @default
	            Default case... --}}
	@endswitch


	{{-- expr --}}
@endif

@endsection
