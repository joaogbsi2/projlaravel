@extends('layouts.principal')

@section('titulo', 'departamentos')

@section('conteudo')

	<h3>Departamentos</h3>

	<ul>
		<li>computadores</li>
		<li>eletronicos</li>
		<li>acessorios</li>
		<li>roupas</li>
	</ul>

	@alerta( ['titulo'=>'erro fatal', 'tipo'=>'info'])
		<p><strong>Erro Inesperado</strong></p>
		<p>ocorreu um erro inesperado</p>
	@endalerta
	@alerta( ['titulo'=>'erro fatal', 'tipo'=>'error'])
		<p><strong>Erro Inesperado</strong></p>
		<p>ocorreu um erro inesperado</p>
	@endalerta
	@alerta( ['titulo'=>'erro fatal', 'tipo'=>'success'])
		<p><strong>Erro Inesperado</strong></p>
		<p>ocorreu um erro inesperado</p>
	@endalerta
	@alerta( ['titulo'=>'erro fatal', 'tipo'=>'warning'])
		<p><strong>Erro Inesperado</strong></p>
		<p>ocorreu um erro inesperado</p>
	@endalerta
	
	
@endsection